Live Chat Support System
====================

A base for a live chat system.

For demonstration purposes it currently uses an AI API for response and is **very** lightly trained but isn't always great at its responses.

Best viewed in Chrome (but should work OK with other modern browsers too. - CURRENTLY NOT MOBILE!)

WORKING DEMO: https://kickflip.ga/livesupport/index.html

**For a somewhat decent response, follow this example**:

1. Put in your name.
2. Select "Account" as the issue type.
3. Say something like "I am locked out of my account."
4. Then say "thanks" (Some other variations also cause it to say goodbye)
5. Say something like "goodbye", "bye", "see ya", "later" etc...

You can also type "cause error" to see a fake error message or what an error would like look should one occur with the connection.