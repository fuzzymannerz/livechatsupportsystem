/*////////////////////////////////////////////
///    Live Chat Support System  | 2018    ///
///       github.com/fuzzymannerz          ///
*/////////////////////////////////////////////


// Set some user variables
var userName = "";
var userIssue = "some";
var userInput = "";

var APIAccessToken = "S6BDAEI66RDGTDDOO6WPN7BKK3JQRNUU";


// When page loads, show only user input
$("#chatContent").hide();


// Submit initial user form on button click
$("#userInformation").submit(function(event) {
    event.preventDefault(); // Stops page reloading due to default form submission
    
    userName = $("#userName").val();
    userIssue = $("#userIssue").val();
    
    // Hide initial information request and show chat window stuff
    $("#initialContent").fadeOut(function(){
        $("#chatContent").fadeIn("slow");
        $("#chatInput").focus();   
    });

    //Initial support response message
    $("#chatWindow").append("<p id=\"chatMessage\" class=\"supportChatMessage\"> <span class=\"userProfile\">Support</span> Hi there " + userName + ". I see you're having " + userIssue.toLowerCase() + " issues. How can I help you today?</p>").children(':last').hide().fadeIn(500);
    
    // Connect to the API to test connection and display status
    $.ajax({
      url: "https://api.wit.ai/message",
      data: {
        'q': "",
        'access_token' : APIAccessToken,
      },
      dataType: 'jsonp',
      method: 'GET',
        
        success: function(data) {
        ConnectedStatus(true);
    },
        error: function(error){
        Error(error);
    }
    });
    

});

// Function to scroll the chat window
function ScrollChat(){
    var messageHeight = 0;
    
    // Get the height of each chat message
    $('#chatWindow p').each(function(){
        messageHeight += $(this).height() + 40; // +40 to account for the padding
    });
    
    messageHeight += "";
    
    // Scroll the chat window to fit latest message
    $("#chatWindow").animate({scrollTop: messageHeight});
}

// Function to disable the submit button for X seconds
function ToggleSubmittable(seconds){
    $("#chatButton").attr('disabled','disabled');
    
    setTimeout(function(){
        $("#chatButton").attr('disabled',false);
    }, seconds * 1000);
}

// Error message
function Error(error){
    ConnectedStatus(false);
    
    $("#chatWindow").append("<p id=\"chatMessage\" class=\"errorChatMessage\"> <span class=\"userProfile\">ERROR</span>" + error + ".</p>").children(':last').hide().fadeIn(500);
    
    // Scroll to keep the latest message in view
    ScrollChat();
}

// Update the status of the connection
function ConnectedStatus(c){
    if (c == true){
       $("#chatStatus").html("<span class=\"chatStatusPositive\"><span class=\"bullet\">&#9646;</span> Connected to agent.</span>"); 
    }
    else{
        $("#chatStatus").html("<span class=\"chatStatusNegative\"><span class=\"bullet\">&#9646;</span> Disconnected from agent.</span>");
    }
    
}

// Contact witAI API
function RunAPI(){
    
    $.ajax({
      url: "https://api.wit.ai/message",
      data: {
        'q': userInput,
        'access_token' : APIAccessToken,
      },
      dataType: 'jsonp',
      method: 'GET',
      success: function(data) {
                
          // If user said a greeting
          if (data.entities.greetings && data.entities.greetings[0].confidence > 0.9){
              Respond("Hello there " + userName + ". :)");
          }
          
          // If user asks a question
          if (data.entities.question){
              
              // If user asks for help
              if (data.entities.question[0].value == "help"){
                  Respond("Sure I can!");
                  }
              
              // If user asks "how are you?"
                if (data.entities.question[0].value == "howami"){
                    Respond("Very well thanks " + userName + "!");
                }
          }
          
          // If user says thank you
          if (data.entities.thanks){
              Respond("No worries " + userName + ", you're very welcome!");
          }
          
          // If user says goodbye
          if (data.entities.bye){
              Respond("Bye " + userName + ", have a nice day!");
          }
          
          // If user mentions a problem
          if (data.entities.problem){ 
              
              // If user mentions a problem but not what it is
              if (data.entities.problem[0].value == "notknown"){
                  Respond("What seems to be the problem " + userName + "?");
              }
              
              // If user mentions account is locked out
              if (data.entities.problem[0].value == "lockedout"){
                  Respond("I can help you gain access to your account again in no time!");
              }
              
              // If user mentions account issue in general
              if (data.entities.problem[0].value == "accountissue"){
                  Respond("Sorry to hear this, I can help you with your account.");
              }
          }

      },
        error: function(error){
            Error(error);
        }
        
    });
}

// Function for support response messages
function Respond(message){
    
    // Set agent typing
    $("#chatStatus").html("<span class=\"chatStatusPositive\"><span class=\"bullet\">&#9646;</span> Agent is typing...</span>");
    
    // Short time out to make response seem a bit more humanistic and natural feeling
    setTimeout(function(){
        $("#chatWindow").append("<p id=\"chatMessage\" class=\"supportChatMessage\"> <span class=\"userProfile\">Support</span> " + message + "</p>").children(':last').hide().fadeIn(500);
        
        // Scroll to keep the latest message in view
        ScrollChat();
        
        ConnectedStatus(true);
    }, 1500);

}

// Submit chat form on button click
$("#inputBar").submit(function(event) {
    event.preventDefault(); // Stops page reloading due to default form submission
    
    userInput = $("#chatInput").val();
    
    // Update the chat window with user message
    $("#chatWindow").append("<p id=\"chatMessage\" class=\"userChatMessage\"> <span class=\"userProfile\">" + userName + "</span> " + userInput + "</p>").children(':last').hide().fadeIn(500);
    
    // Scroll to keep the latest message in view
    ScrollChat();
    
    // Clear the input box on send
    $("#chatInput").val("");
    
    // Contact the API
    RunAPI();
    
    // Cause an error for style debug
    if (userInput == "cause error"){
        Error("This is a fake error message.");
        }
    
    //Disable submit button for 2 seconds to prevent slowdown caused by potential spamming
    ToggleSubmittable(2);

});

// Disconnect from chat
function Disconnect(){
    // Inform user of closed chat
    $("#chatStatus").html("<span class=\"chatStatusNegative\"><span class=\"bullet\">&#9646;</span> Disconnected. You may now close this chat window.</span>"); 
    
    $("#connectionButtons").fadeOut(); // Remove disconnect button
    $("#inputBar").fadeOut(); // Disable chat abilities
    $("#chatWindow").css("opacity", "0.3"); // Change chat window opacity
}